export default function (state = [], action) {
    switch (action.type) {
        case "BOOKS_DATA":
            return action.data;
        default:
            return state;
    }
}