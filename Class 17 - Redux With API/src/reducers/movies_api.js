export default function (state = [], action) {
    switch (action.type) {
        case "MOVIES_API":
            return action.data;
        default:
            return state;
    }
}