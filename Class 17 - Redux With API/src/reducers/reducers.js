import {
    combineReducers
} from 'redux';
import BooksReducer from "./books";
import MoviesReducer from "./movies";
import NameCanBeAnything from "./students";
import MoviesAPI from "./movies_api";

let reducer_data = combineReducers({
    books: BooksReducer,
    movies: MoviesReducer,
    nameAAA: NameCanBeAnything,
    moviesFromAPI: MoviesAPI
});

export default reducer_data;