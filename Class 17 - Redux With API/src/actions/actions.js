import axios from "axios";

export function BooksData() {
    return {
        type: "BOOKS_DATA",
        data: [{
            key: 1,
            name: "Harry Potter"
        }, {
            key: 2,
            name: "Mistborn"
        }, {
            key: 3,
            name: "Malazan"
        }]
    }
}

export function MoviesData() {
    return {
        type: "MOVIE_DATA",
        data: [{
            key: 1,
            name: "Harry Potter"
        }, {
            key: 2,
            name: "Dark Knight"
        }, {
            key: 3,
            name: "Truman Show"
        }]
    }
}

export function StudentData() {
    return {
        type: "STUDENT_DATA",
        data: [{
            key: 1,
            name: "Praveena"
        }, {
            key: 2,
            name: "Navya"
        }, {
            key: 3,
            name: "Roja"
        }]
    }
}

export function MoviesDataFromAPI() {
    return (dispatch) => {
        axios.get("https://api.themoviedb.org/3/movie/popular?api_key=99c26306be3d1ce05090f2450ff90c6e&language=en-US&page=1").then(res => {
            console.log(res);
            dispatch({
                type: "MOVIES_API",
                data: res.data.results
            })
        })
    }
}