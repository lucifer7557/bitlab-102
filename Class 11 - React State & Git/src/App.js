import React from 'react'; // Importing React from 'react' package.
import StateFul from './StateFul';

let App = (props) => {
  return <div>
    <h3>Hello</h3>
    <StateFul villain="Turbo" />
  </div>
} // ES6 or Arrow Function

// function App(){
//   return <h1>Hello There</h1>
// }

export default App;

// Functional Based Component
// You have to write export default for each component if you want to use that in the application.
// For every component to work you have to import react at the start of the file.