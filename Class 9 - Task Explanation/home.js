let apiData;

fetch("https://api.themoviedb.org/3/movie/popular?api_key=99c26306be3d1ce05090f2450ff90c6e&language=en-US&page=1").then(function (data) {
    return data.json();
}).then(function (res) {
    apiData = res.results;
    paintData(res.results);
})

function paintData(data) {
    let myDiv = document.getElementById("myCards");
    let b = [];
    myDiv.innerHTML = "";
    for (let i = 0; i < data.length; i++) {
        let src = "https://image.tmdb.org/t/p/w185_and_h278_bestv2";
        //rating     
        let avg = data[i].vote_average;

        b.push(avg);
        // if (b[i] > 6) {
        let src1 = data[i].poster_path;
        let src3 = src.concat(src1);
        var date = new Date(data[i].release_date);
        var monthNames = [
            "JANUARY", "FEBRUARY", "MARCH",
            "APRIL", "MAY", "JUNE", "JULY",
            "AUGUST", "SEPTEMBER", "OCTOBER",
            "NOVEMBER", "DECEMBER"
        ];

        var monthIndex = date.getMonth();
        var year = date.getFullYear();

        var conDate = monthNames[monthIndex].concat(year);

        let myHTML = '<div class="parentDiv">\
        <img  src = "' + src3 + '" onmouseover="hover(' + i + ')" onmouseout="hoverOut(' + i + ')"> \
        <div class="hover" id="hover' + i + '">Hello</div>\
        <span class = "title" > ' + data[i].original_title + ' </span>\
        <h4> ' + conDate + ' </h4>\
        <p> ' + data[i].overview + '</p>\
        <p>' + b[i] + '</p>\
        </div>';
        myDiv.innerHTML += myHTML;
        // } else {}
    }
}

function aboveSix() {
    let data = apiData;
    data = data.filter(function (zdas) {
        return zdas.vote_average >= 6;
    })
    paintData(data);
}

function belowSix() {
    let data = apiData;
    data = data.filter(function (e) {
        return e.vote_average < 6;
    })
    paintData(data);
}

function hover(i) {
    document.getElementById("hover" + i).classList += " DB";
    // alert("Hovered");
}

function hoverOut(i) {
    console.log("Out");
    document.getElementById("hover" + i).classList += " DN";
}