import React from 'react';
import './Movie.css';

let Movie = (props) => {
    return <div className="divMovie" key={props.obj.id}>
        <img src={"https://image.tmdb.org/t/p/w185_and_h278_bestv2" + props.obj.poster_path} alt={props.obj.title} />
        <h3>{props.obj.title}</h3>
    </div>
}

export default Movie;