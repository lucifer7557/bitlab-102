let first = "Hello";
let last = "World";
console.log(first + " " + last); // string concatenation by +

let bigString = "Hello*World*There*You*Are";
let idx = bigString.indexOf("T"); // Gives you the position of the characters in the string.
console.log(idx);
console.log(bigString.split("*")); // splits the element by the characters that is mentioned and returns an array

let arr = ["Hello", "World", "There", "You", "Are"];
console.log(arr.join("-------")); // Joins the array with the characters mentioned and returns a string
console.log(bigString.length); // .length gives the length of the array or string.

for (let i = 0; i < arr.length; i++) { // For Loop
    console.log(arr[i] + " ***");
    // Hello ***
    // World ***
    // There ***
    // You ***
    // Are ***
}

// arr.push("Nope"); // Pushes an element at last position
// arr.pop(); // Removes an element from last position
let asdfg = arr.splice(2, 2);
console.log(arr, asdfg); // To access element at position 3
console.log(arr.indexOf("Are")); // To get the position of the element, if element doesn't exist returns -1.

let myObject = {
    "full name": "Batman",
    "origin": "DC",
    year: 1990,
    children: ["Robin", "Tim"],
    alive: true,
    childObj: {
        hello: "There"
    }
}

let variableName = "full name";
console.log(myObject.year);
console.log(myObject[variableName]);

let empObj = {};

let myArrayOfObjects = [{
    key: 1,
    name: "Batman"
}, {
    key: 2,
    name: "Flash"
}, {
    key: 3,
    name: "DD"
}, {
    key: 4,
    name: "Spidey"
}]

// Hello Batman
// Hello Flash
// Hello DD
// Hello Spidey

console.log(myArrayOfObjects[1]);

for (let j = 0; j < myArrayOfObjects.length; j++) {
    console.log("Hello " + myArrayOfObjects[j].name);
    // console.log(myArrayOfObjects[j]);
}

// Convert to string to number
console.log(parseInt("2"));

// convert to number to string
let num = 12.33;
console.log(num.toString());

// convert to object to string
console.log(JSON.stringify({
    name: "Hello",
    there: "There"
}))

// convert to string to object
console.log(JSON.parse('{name:"Hello",there:"There"}'));