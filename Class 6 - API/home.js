console.log("Helo");

// fetch('https://stumped.herokuapp.com/api/posts').then(function (res) { // Fetching the data from API Url, .then is used to say execute this when this is done.
//     //console.log(res);
//     return res.json();
// }).then(function (result) {
//     console.log(result);
//     paintTheData(result);
// });

console.log("Finished");

let arrayOfObjects = [{
    key: 81,
    name: "Bats"
}, {
    key: 32,
    name: "Flash"
}, {
    key: 53,
    name: "DD"
}, {
    key: 40,
    name: "Spidey"
}]; //[1,"asa",true,[],{}]

let names = arrayOfObjects.map(function (abc) { // Loops through all the data and returns all the data but you can manipulate it.
    return "I am " + abc.name;
})

let fil = arrayOfObjects.filter(function (xyz) { // Loops through all the data and returns you the data only when the condition is satisfied. Can't manipulate data.
    return xyz.key > 2;
})

console.log(names, fil);

// ["I am Bats","I am Flash","I am DD","I am Spidey"]

function paintTheData(data) {
    let myData = document.getElementById("myData"); // Getting the element

    for (let i = 0; i < data.length; i++) {
        let heading = document.createElement("h2"); // Creating the element with given tag
        heading.innerHTML = data[i].title;

        let img = document.createElement("img");
        img.src = data[i].mainImage;
        img.style.width = "200px";
        img.style.height = "200px";
        // heading.style.color = "red";

        let div = document.createElement("div");
        div.appendChild(img);
        div.appendChild(heading);

        myData.appendChild(div); // Adding child to parent.
    }

}

let numbers = [22, 44, 11, 5, 9, 21, 36, 40];
let strings = ["A", "Z", "B", "C", "M"];
let sortNumb = numbers.sort(function (a, b) {
    return a - b;
});
let sortAlp = strings.sort();
console.log(sortNumb, sortAlp);