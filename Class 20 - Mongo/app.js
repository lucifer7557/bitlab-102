let express = require('express');
let bodyParser = require('body-parser');
let cors = require('cors');
// let dummy = require('./dummyFile');
let mongoData = require('./mongoData');

let movie = mongoData.movie;
let tvModel = mongoData.tvObject;

let app = express();

app.use(bodyParser.json());
app.use(cors());

app.get('/', (req, res) => {
    movie.find({}, (err, docs) => {
        console.log(docs);
        res.send({
            docs
        })
    })
})


app.post('/addMovie', (req, res) => {
    // let name = req.body.name;
    // let rating = req.body.rating;
    // let director = req.body.director;
    let {
        name,
        rating,
        director
    } = req.body;
    let obj = new movie({
        // name: name,
        rating: rating,
        director: director
    })
    obj.save().then(() => {
        res.send({
            msg: "Success"
        })
    }).catch((err) => {
        res.send({
            msg: "Failed " + err
        })
    })

})

app.get('/movie/:name', (req, res) => {
    let movieName = req.params.name;
    movie.find({
        name: movieName
    }, (err, docs) => {
        console.log(docs);
        res.send({
            docs
        })
    })
})

// Update moviename
// Delete movie


// let obj = new movie({
//     name: "Rangasthalam",
//     rating: 10,
//     director: "Sukumar"
// })

// obj.save();

// let tvObj = new tvModel({
//     name: "Friends",
//     genre: "Comedy"
// })

// tvObj.save();

app.listen('5000', () => {
    console.log("Node Successful");
})