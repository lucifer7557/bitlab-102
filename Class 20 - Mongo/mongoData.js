let mongoose = require("mongoose");

mongoose.connect("mongodb://localhost:27017/DBName");

let movie = mongoose.model('Movie', {
    name: {
        type: String,
        required: [true, 'You are wrong!!!']
    },
    rating: Number,
    director: String
})

let tv = mongoose.model('TV', {
    name: String,
    genre: String
})

module.exports = {
    movie: movie,
    tvObject: tv
}