import React from 'react';
import { Switch, Link, Route } from 'react-router-dom';

import Hello from './Hello';
import Pro from './Pro';

let About = () => {
    return <div>
        <h2>I'm in About Page</h2>

        <div><Link to="/about/hello">Hello</Link></div>
        <div><Link to="/about/pro">Pro</Link></div>

        <Switch>
            <Route path="/about/hello" component={Hello} />
            <Route path="/about/pro" component={Pro} />
        </Switch>
    </div>
}

export default About;