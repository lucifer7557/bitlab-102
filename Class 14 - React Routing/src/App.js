import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import { Switch, Link, Route } from 'react-router-dom';
import Home from './Home';
import About from './About';
import Contact from './Contact';
import User from './User';
import NotFound from './NotFound';

class App extends Component {
  render() {
    return (
      <div className="App">
        <nav class="navbar navbar-expand-lg navbar-light bg-light">
          <a class="navbar-brand" href="#">Navbar</a>
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
          </button>
          <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
            <div class="navbar-nav">
              <Link to="/" class="nav-item nav-link">Home <span class="sr-only">(current)</span></Link>
              <Link to="/about" class="nav-item nav-link">About</Link>
              <Link to="/contact" class="nav-item nav-link">Contact</Link>
              <a href="https://google.com" class="nav-item nav-link">Google</a>
            </div>
          </div>
        </nav>
        <div>
          <Switch>
            <Route exact path="/" component={Home} />
            <Route path="/about" component={About} />
            <Route path="/contact" component={Contact} />
            <Route path="/posts/:article" component={User} />
            <Route component={NotFound} />
            {/* For Dynamic Routes */}
          </Switch>
        </div>
      </div>
    );
  }
}

export default App;
