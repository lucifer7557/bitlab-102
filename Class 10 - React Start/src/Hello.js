import React from 'react';
import App from './App';
import Second from './Second';

let Hello = () => {
    return <div>
        <h1>Hello Kiddo</h1>
        <App ab="Vanellope" age="10" />
        <App ab="Ralph" age="30" />
        <App ab="King" age="30" />
        <App ab="Ab" age="30" />
        <App ab="as" age="30" />
    </div>
}

export default Hello;