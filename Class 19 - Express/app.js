const express = require("express");
const fs = require("fs");
const cors = require("cors");
const bodyParser = require('body-parser');

let app = express();
app.use(bodyParser.json());
app.use(cors());

app.get('/', (req, res) => {
    let oldData = fs.readFileSync('./dummy.json'); // Fetching Data from the file
    console.log(oldData);
    oldData = JSON.parse(oldData);
    res.send({
        data: oldData
    })
})

app.post('/add', (req, res) => {
    let oldData = fs.readFileSync('./dummy.json'); // Fetching Data from the file
    oldData = JSON.parse(oldData); // Data will be in buffers so, we need to convert it to JSON
    oldData.push({
        name: req.body.name,
        rating: req.body.rating
    });
    fs.writeFileSync('./dummy.json', JSON.stringify(oldData)); // When you add data to a file it will try to convert it to string.
    res.send({
        msg: "Success"
    })
})

app.get('/:name', (req, res) => {
    console.log(req.params.name);
    let fileData = fs.readFileSync('./dummy.json');
    fileData = JSON.parse(fileData);
    console.log(fileData);
    let myData = fileData.filter(function (e) {
        return e.name == req.params.name;
    })
    res.send({
        myData: myData
    })
})

app.post('/delete/:name', (req, res) => {
    let movieName = req.params.name;
    let fileData = fs.readFileSync('./dummy.json');
    fileData = JSON.parse(fileData); // 7 Objects
    fileData = fileData.filter((e) => {
        return e.name != movieName;
    }) // 6 Objects

    fs.writeFileSync('./dummy.json', JSON.stringify(fileData)); // When you add data to a file it will try to convert it to string.
    res.send({
        msg: "Success"
    })
})


// get, post, put, delete
// CRUD - Create, Read, Update and Delete

app.listen(5000, () => {
    console.log("Port Started");
})

// Get the all the movies
// Delete One movie by name
// Update one movie by name



// To run a node application node app.js