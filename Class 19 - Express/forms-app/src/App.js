import React, { Component } from 'react';
import axios from "axios";
import './App.css';

class App extends Component {
  constructor() {
    super();

    this.state = { name: "", rating: "" };
  }

  addMovie() {
    axios.post('http://localhost:5000/add', {
      name: this.state.name,
      rating: this.state.rating
    }).then((res) => {
      console.log(res);
    })
  }

  render() {
    return (
      <div className="App">
        <input type="text" placeholder="name" value={this.state.name} onChange={(ev) => { this.setState({ name: ev.target.value }) }} />
        <input type="text" placeholder="rating" value={this.state.rating} onChange={(ev) => { this.setState({ rating: ev.target.value }) }} />
        <button onClick={() => this.addMovie()}>Add</button>
      </div>
    );
  }
}

export default App;
