fetch('http://demo0879201.mockable.io').then(function (res) { // Fetching the data from API Url, .then is used to say execute this when this is done.
    //console.log(res);
    return res.json();
}).then(function (result) {
    console.log(result);
    //showTheOutput(result);
    showTheOutputSecondWay(result);
});


function showTheOutput(data) {
    let myDiv = document.getElementById("myData");
    myDiv.style.padding = "50px";
    let dataArr = data.objects;
    for (let i = 0; i < dataArr.length; i++) {
        let parent = document.createElement("div");
        parent.style.borderBottom = "1px solid blue";
        parent.style.marginTop = "10px";

        let image = document.createElement("img");
        image.src = dataArr[i].employer.profile_image_src;
        image.style.width = "50px";
        image.style.verticalAlign = "middle";

        let companyName = document.createElement("div");
        companyName.innerHTML = dataArr[i].employer.company_name;
        companyName.style.display = "inline-block";
        companyName.style.marginLeft = "20px";
        companyName.style.verticalAlign = "middle";

        let note = document.createElement("p");
        note.innerHTML = dataArr[i].employer.instahyre_note;

        parent.appendChild(image);
        parent.appendChild(companyName);
        parent.appendChild(note);

        myDiv.appendChild(parent);
    }
}

function showTheOutputSecondWay(data) {
    let myDiv = document.getElementById("myData");
    let dataArr = data.objects;
    for (let i = 0; i < dataArr.length; i++) {
        // myDiv.innerHTML += "";
        let myHTML = '<div class="parentDiv">\
        <img src = "' + dataArr[i].employer.profile_image_src + '" > \
        <div class = "companyName" > ' + dataArr[i].employer.company_name + ' </div>\
        <p> ' + dataArr[i].employer.instahyre_note + ' </p>\
        </div>';
        myDiv.innerHTML += myHTML;

    }
}