const os = require("os"); // Operating System
const fs = require("fs"); // File System
const _ = require("lodash");
// path, http, 

let arr = [10, 21, 0, 1, 3, 45, 23, 66];

console.log(_.max(arr));

var evens = _.remove(arr, function (n) {
    return n % 2 == 0;
});

console.log(evens);

console.log(os.userInfo());

let dummy = fs.readFile("./dummy.json", function (err, res) {
    if (err) {
        console.log(err);
        return false;
    }
    // console.log(JSON.parse(res));
    let data = JSON.parse(res);
    data.push({
        key: 7,
        name: "DD"
    });
    fs.writeFile("./dummy.json", JSON.stringify(data), function (err, res) {

    })

    console.log(dummy);
});